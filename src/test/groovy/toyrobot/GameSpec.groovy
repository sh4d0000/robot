package toyrobot

import spock.lang.Specification
import spock.lang.Subject
import spock.lang.Unroll

class GameSpec extends Specification {

    @Subject
    ToyRobotGame toyRobotGame = new ToyRobotGame()

    def setup() {
        def tabletop = new Tabletop(5)
        def factory = new CommandsFactory()
        toyRobotGame = new ToyRobotGame(commandsFactory: factory, tabletop: tabletop)
    }

    def 'a robot is place on the initial point of the tabletop'() {
        given:
        def instructions = '''
            PLACE 0,0,NORTH 
            REPORT
        '''

        when:
        toyRobotGame.play instructions

        then:
        toyRobotGame.output() == "0,0,NORTH\n"
    }

    @Unroll
    def 'a robot rotates on the tabletop'() {
        given:
        def instructions = """
            PLACE 0,0,NORTH 
            $rotate
            REPORT
        """

        when:
        toyRobotGame.play instructions

        then:
        toyRobotGame.output() == "0,0,$orientation\n"

        where:
        rotate  | orientation
        'LEFT'  | 'WEST'
        'RIGHT' | 'EAST'
    }

    def 'a robot moves on the tabletop'() {
        given:
        def instructions = '''
            PLACE 2,2,SOUTH
            MOVE 
            REPORT
            MOVE
            REPORT
        '''
        when:
        toyRobotGame.play instructions

        then:
        toyRobotGame.output() == "2,1,SOUTH\n2,0,SOUTH\n"
    }

    def 'full game'() {
        def instructions = '''
            PLACE 0,0,NORTH
            MOVE
            REPORT
            PLACE 0,0,NORTH
            LEFT
            REPORT
            PLACE 1,2,EAST
            MOVE
            MOVE
            LEFT
            MOVE
            REPORT
        '''

        when:
        toyRobotGame.play instructions

        then:
        toyRobotGame.output() == """0,1,NORTH
0,0,WEST
3,3,NORTH
"""
    }

    def 'should ignore the first instructions if not a PLACE'() {
        given:
        def instructions = '''
            MOVE 
            REPORT
            LEFT
            REPORT
            PLACE 2,2,SOUTH
            REPORT
            MOVE
            REPORT
        '''
        when:
        toyRobotGame.play instructions

        then:
        toyRobotGame.output() == "2,2,SOUTH\n2,1,SOUTH\n"
    }

    def 'should handle an empty input'() {
        when:
        toyRobotGame.play instructions

        then:
        toyRobotGame.output() == ''

        where:
        instructions << ['', null, ' ']
    }

    def 'should warn wrong instructions'() {
        when:
        toyRobotGame.play "REPORT\nwrong command"

        then:
        toyRobotGame.output() == 'instuction >WRONG COMMAND< is not a valid command'
    }

    def 'should warn wrong isnstructions'() {
        when:
        toyRobotGame.play """
            PLACE 0,0,NORTH
            REPORT
            PLACE a,0,NORTH
        """

        then:
        toyRobotGame.output().startsWith 'instuction >PLACE A,0,NORTH< does not match pattern:'
    }

}
