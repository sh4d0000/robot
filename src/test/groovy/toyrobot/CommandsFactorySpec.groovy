package toyrobot

import spock.lang.Specification
import spock.lang.Subject
import spock.lang.Unroll

import static toyrobot.Orientation.NORTH

class CommandsFactorySpec extends Specification {

    @Subject CommandsFactory factory = new CommandsFactory()

    @Unroll
    def 'should make a place command'() {
        when:
        def commands = factory.make instruction

        then:
        commands == [new Place(new Position(x: 0, y: 0, orientation: NORTH))]

        where:
        instruction << ["PLACE 0,0,NORTH", "PLACE  0,  0,  NORTH", "\nPLACE  0,  0,  NORTH\n", "place 0,0,north",
                        "PLACE 0,0,NORTH "]
    }

    @Unroll
    def 'should warn a invalid pattern for a place command'() {
        when:
        factory.make instruction

        then:
        def invalidCommand = thrown InvalidCommandException
        invalidCommand.pattern == Place.PATTERN
        invalidCommand.instruction == instruction.toUpperCase()

        where:
        instruction << ["PLACE a, 2, NORTH", "PLACE 3, b, NORTH", "PLACE 4,2, SWEST", "PLACE 0,0,NORTH PLACE 0,0,NORTH"]
    }

    def 'should warn a missing command'() {
        when:
        factory.make "WRONG 5,2,NORTH"

        then:
        def missingCommandException = thrown MissingCommandException
        missingCommandException.instruction == "WRONG 5,2,NORTH"
    }

    @Unroll
    def 'should make a report command'() {
        when:
        def commands = factory.make "REPORT"

        then:
        commands == [new Report()]
    }
}
