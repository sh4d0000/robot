package toyrobot

import spock.lang.Specification
import spock.lang.Subject
import spock.lang.Unroll

import static toyrobot.Orientation.EAST
import static toyrobot.Orientation.NORTH
import static toyrobot.Orientation.SOUTH
import static toyrobot.Orientation.WEST

class TabletopSpec extends Specification {

    @Subject
    Tabletop tabletop

    def setup() {
        tabletop = new Tabletop(5)
    }

    def 'should place the robot'() {
        given:
        def position = new Position(x: 0, y: 0, orientation: NORTH)

        when:
        tabletop.placeTo position

        then:
        tabletop.position == position
    }

    def 'should report the robot position'() {
        given:
        def position = new Position(x: 0, y: 2, orientation: SOUTH)
        tabletop.placeTo position

        expect:
        tabletop.report() == position
    }

    @Unroll
    def 'should move the robot'() {
        given:
        def position = new Position(x: 2, y: 2, orientation: orientation)
        tabletop.placeTo position

        when:
        tabletop.move()

        then:
        tabletop.report() == new Position(x: x, y: y, orientation: orientation)

        where:
        orientation | x | y
        SOUTH       | 2 | 1
        NORTH       | 2 | 3
        EAST        | 3 | 2
        WEST        | 1 | 2
    }

    @Unroll
    def 'should turn the robot on the left'() {
        given:
        def position = new Position(x: 2, y: 2, orientation: orientation)
        tabletop.placeTo position

        when:
        tabletop.left()

        then:
        tabletop.report() == new Position(x: 2, y: 2, orientation: expectedOrientation)

        where:
        orientation | expectedOrientation
        NORTH       | WEST
        WEST        | SOUTH
        SOUTH       | EAST
        EAST        | NORTH
    }

    @Unroll
    def 'should turn the robot on the right'() {
        given:
        def position = new Position(x: 2, y: 2, orientation: orientation)
        tabletop.placeTo position

        when:
        tabletop.right()

        then:
        tabletop.report() == new Position(x: 2, y: 2, orientation: expectedOrientation)

        where:
        orientation | expectedOrientation
        NORTH       | EAST
        EAST        | SOUTH
        SOUTH       | WEST
        WEST        | NORTH
    }

    @Unroll
    def 'the robot should not fall off the tabletop'() {
        given:
        tabletop.placeTo position

        when:
        tabletop.move()

        then:
        tabletop.report() == position

        where:
        position << [
                new Position(x: 0, y: 1, orientation: WEST),
                new Position(x: 1, y: 4, orientation: NORTH),
                new Position(x: 1, y: 0, orientation: SOUTH),
                new Position(x: 4, y: 1, orientation: EAST)
        ]
    }
}
