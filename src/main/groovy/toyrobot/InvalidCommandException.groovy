package toyrobot

import java.util.regex.Pattern

class InvalidCommandException extends Exception {
    Pattern pattern
    String instruction

    @Override
    String getMessage() {
        "instuction >$instruction< does not match pattern: >$pattern<"
    }
}

class MissingCommandException extends Exception {
    String instruction

    @Override
    String getMessage() {
        "instuction >$instruction< is not a valid command"
    }
}
