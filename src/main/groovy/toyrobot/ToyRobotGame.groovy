package toyrobot

class ToyRobotGame {

    CommandsFactory commandsFactory
    Tabletop tabletop
    StringBuilder output = new StringBuilder()

    static void main(String[] args) {
        def game = new ToyRobotGame(tabletop: new Tabletop(5), commandsFactory: new CommandsFactory())

        def instructions = ToyRobotGame.class.classLoader.getResourceAsStream('sample.instructions').text
        game.play instructions

        print game.output()
    }

    void play(String instructions) {
        try {
            if (!instructions) return

            def commands = commandsFactory.make instructions
            execute commands
        } catch (InvalidCommandException | MissingCommandException ex) {
            append ex.message
        }
    }

    void execute(List<Command> commands) {
        commands.dropWhile { !(it in Place) }*.execute(tabletop, this)
    }

    String output() {
        output.toString()
    }

    protected void append(String message) {
        output.append message
    }
}