package toyrobot

import groovy.transform.Canonical

import java.util.regex.Pattern


class CommandsFactory {

    Map<String, Closure<Command>> makers = [
            'PLACE' : { String instruction ->
                Command command = null

                instruction.eachMatch(Place.PATTERN) { String match, x, y, orientation ->
                    def position = new Position(x: x as int, y: y as int, orientation: orientation)
                    command = new Place(position)
                }

                if (!command) throw new InvalidCommandException(pattern: Place.PATTERN, instruction: instruction)

                return command
            },
            'REPORT': { new Report() },
            'MOVE'  : { new Move() },
            'LEFT'  : { new Left() },
            'RIGHT' : { new Right() }

    ]

    List<Command> make(String instructions) {
        List<Command> commands = []

        instructions.toUpperCase().trim().eachLine {
            String line = it.trim()

            def firstWord = line.tokenize().first()
            if (!makers.keySet().contains(firstWord)) throw new MissingCommandException(instruction: line)

            commands << makers[firstWord].call(line)
        }

        return commands
    }

}

@Canonical
abstract class Command {

    abstract void execute(Tabletop tabletop, ToyRobotGame game)
}

class Place extends Command {

    public static final Pattern PATTERN = Pattern.compile "^PLACE\\s+(\\d+),\\s*(\\d+),\\s*(NORTH|SOUTH|EAST|WEST)\$"
    Position position

    Place(Position position) {
        this.position = position
    }

    @Override
    void execute(Tabletop tabletop, ToyRobotGame game) {
        tabletop.placeTo position
    }
}

class Report extends Command {

    @Override
    void execute(Tabletop tabletop, ToyRobotGame game) {
        def position = tabletop.report()
        game.append "$position.x,$position.y,$position.orientation\n"
    }
}

class Move extends Command {

    @Override
    void execute(Tabletop tabletop, ToyRobotGame game) {
        tabletop.move()
    }
}

class Left extends Command {

    @Override
    void execute(Tabletop tabletop, ToyRobotGame game) {
        tabletop.left()
    }
}

class Right extends Command {

    @Override
    void execute(Tabletop tabletop, ToyRobotGame game) {
        tabletop.right()
    }
}