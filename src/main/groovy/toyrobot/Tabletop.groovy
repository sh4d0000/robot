package toyrobot

import groovy.transform.Immutable

import static toyrobot.Orientation.NORTH

class Tabletop {

    Position position
    int dimension

    Tabletop(int dimension) {
        this.position = new Position(0, 0, NORTH)
        this.dimension = dimension
    }

    def placeTo(Position position) {
        this.position = position
    }

    Position report() {
        position
    }

    void move() {
        def newPosition = position.move(position)
        if (newPosition.isOutOfBoundary(dimension)) return

        position = newPosition
    }

    void left() {
        position = position.copyWith orientation: position.orientation.next()
    }

    void right() {
        position = position.copyWith orientation: position.orientation.previous()
    }
}

@Immutable(copyWith = true)
class Position {
    int x
    int y
    Orientation orientation

    boolean isOutOfBoundary(int dimension) {
        x < 0 || x + 1 > dimension || y < 0 || y + 1 > dimension
    }

    Position move(Position position) {
        orientation.move(position)
    }
}

enum Orientation {
    SOUTH{
        @Override
        Position move(Position position) {
            position.copyWith y: position.y - 1
        }
    },
    EAST{
        @Override
        Position move(Position position) {
            position.copyWith x: position.x + 1
        }
    },
    NORTH{
        @Override
        Position move(Position position) {
            position.copyWith y: position.y + 1
        }
    },
    WEST{
        @Override
        Position move(Position position) {
            position.copyWith x: position.x - 1
        }
    }

    abstract Position move(Position position)
}